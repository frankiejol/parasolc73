#!/usr/bin/perl -w

use warnings;
use strict;

use Text::Diff;
use File::Copy;

my %RESERVED = map { $_ => 1 } qw(portada fonts etc lowres paper bw);

sub find_docs {
	my $seccio = shift or die "Falta seccio";

	my @docs;
	opendir my $dir,$seccio or die "$! $seccio";
	while (my $file = readdir $dir) {
			next if !-f "$seccio/$file";
			next if $file !~ /^\d+_.*tex$/;
			push @docs,("$seccio/$file");
	}
	closedir $dir;
	return sort @docs;
}
sub do_fix_pagesof {
	my $n_seccio = shift or die "Falta n_seccio";
	my $n_file	 = shift or die "Falta n_file";
	my $tex = shift 	 or die $!;

	my ($pagesof_file) = $tex =~ m{.*/(\d+)_};
	$pagesof_file =~ s/^\d(\d+)/$n_seccio$1/	if length $pagesof_file == 3;
	$pagesof_file = $n_seccio.$pagesof_file if length $pagesof_file < 3;
	$pagesof_file .= $n_file 				if length $pagesof_file < 3;
	$pagesof_file .= '0'					if length $pagesof_file < 3;

	open my $texh,'<',$tex or die "$! $tex";
	open my $texh_new,'>',"$tex.new" or die "$! $tex.new";
	while (<$texh>) {
		if (/^\s*\{\d+\} %pagesof/) {
			$_="{$pagesof_file} %pagesof\n";
		}
		print $texh_new $_;
	}
	close $texh_new;
	close $texh;

	if (diff($tex,"$tex.new")) {
		warn "$tex pagesof diferent\n";
		$tex =~ m{(.*)/(.*)};
		my $swap = "$1/.$2.swp";
		die "ERROR: $tex locked\n" if -e $swap;
		copy("$tex.new",$tex) or die "$! $tex.new -> $tex";
	}
	unlink "$tex.new";
}

sub fix_pagesof {
	my $n_seccio = shift or die "Falta n_seccio";
	my $n_file = 1;
	for my $tex (sort @_) {
		do_fix_pagesof($n_seccio,$n_file++,$tex);
	}
}

sub fix_inputs {

		my $seccio = shift or die $!;
		my $n_seccio = shift or die "Falta n_seccio";

		my @docs = find_docs($seccio) or return;
	
#		fix_pagesof($n_seccio,@docs);
		my $tex = "$seccio/main.tex";
		my $tex_new = "$tex.new";
		open my $texh,'>', $tex_new or die "$! $tex_new";
		for (@docs) {
				print $texh "\\input{$_}\n";
		}
		close $texh;
		if ( diff($tex_new,$tex)) {
			warn "Changed $tex\n";
			copy($tex_new,$tex) or die "$! $tex_new -> $tex";
		}
		unlink $tex_new or die "$! $tex_new";
}

#############################################################

my $n_seccio = 1;
opendir my $dirh ,"." or die $!;
while (my $dir = readdir $dirh){
	next if $dir =~ m{^\.};
	next if !-d $dir;
	next if $RESERVED{$dir};
    next if $dir =~ /angles/i;
	fix_inputs($dir,$n_seccio++);
}
closedir $dirh;
