#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my $DEBUG;
GetOptions ( debug => \$DEBUG );

my ($DIR_BIN) = $0 =~ m{(.*)/};

################################################################
sub cmd_bin {
	my $cmd = shift or die "Falta command";
	return cmd("$DIR_BIN/$cmd",@_);
}

sub cmd {
	my $cmd = shift or die "Falta command";
	my @args = @_;
	for (@args) {
		$cmd .= " $_";
	}
	print "$cmd\n";
	open my $run ,'-|',$cmd or die "$! $cmd";
	while (<$run>) {
			print if /error/i || (/\.tex$/ && !/share/);
	}
	close $run;
}
cmd_bin("esborra_fotos_convert.pl");
cmd("make",@ARGV);
