#!/usr/bin/perl

#use locale;
use warnings;
use strict;
use Carp qw(confess croak);

use Cwd;
use File::Copy;
use IPC::Run qw(run);
use Getopt::Long;
use HTML::Parser;
use HTML::Strip;
#use POSIX qw(locale_h);

use lib 'lib/Parasolc/lib';
use Parasolc qw(crea_tex strip_html traspassa);

my $DIR_FONTS = 'fonts';
my $CHECK_TEX = 0;
my ($VERBOSE,$DEBUG,$TEST,$HELP) = 0;
my $LIBREOFFICE= `which libreoffice`;
chomp $LIBREOFFICE;
die "No trobo libreoffice" if !$LIBREOFFICE;

GetOptions( tex => \$CHECK_TEX
            ,test  => \$TEST
            ,help  => \$HELP
			,debug => \$DEBUG
			,verbose => \$VERBOSE
);
if ($HELP) {
    print "$0 [--test] [--help] [--debug] [--verbose] [--tex]\n"
            ."  test: No fa el traspas, només avisa\n"
            ."  tex: Només comproba els arxius .tex\n"
            ;
}
my %SECCIO;
my ($REVISTA) = getcwd() =~ m{.*/(.*)};



sub busca_seccions {

	my ($tex) = (shift or "$REVISTA.tex");

	my @seccions;
	open P_TEX,"<$tex" or die "$! $tex";
	while (<P_TEX>) {
		s/#.*//;
		my ($type,$include) = m[\\(include|input)\{(.*)/];
		next if ! $include;
#		push @seccions , ( $include) if $type eq 'include';
		$SECCIO{"$include/$include.tex"}++;
	}
	close P_TEX;
}

sub busca_doc {
	my $tex_file = shift or die $!;
	return if $tex_file =~ m{.*/\d+_exemple.tex$};

	open TEX,"<$tex_file" or die "$! $tex_file";
	my ($doc_file,$titol);
    my @doc_file;
	while (<TEX>) {
		($doc_file) = /^%.*doc:\s*(.*)/ if /^%.*doc:/;
		if ( $doc_file  
				&& !-e "fonts/$doc_file" 
                && !-e $doc_file ) {
			warn "No existeix '$doc_file' a $tex_file\n" ;
		}
		if (/columnes/) {
			$titol=<TEX>;
		}
        push @doc_file,($doc_file) if $doc_file;
		last if $doc_file && $titol;
	}
	close TEX;
	print join(",",@doc_file)."\n$titol\n" if $VERBOSE && $doc_file && $titol;
    return if !scalar @doc_file;
	return \@doc_file;
}

sub conte_inputs {
		
	my $tex_file = shift or die ;

	my $inputs=0;
	open TEX,"<$tex_file" or die "$! $tex_file";
	while (<TEX>) {
		s/\%.*//;
		$inputs++ if m{\\input};
	}
	return $inputs;
}

sub es_input {
		my $tex_file = shift or die $!;
		$tex_file =~ s/.tex$//;
		my ($dir) = $tex_file =~ m{(.*)/};
        return if !$dir;

		my $input = 0;
		my $cmd = "grep $tex_file $dir/*tex";
		open GREP , "$cmd|" or die $!;
		while (<GREP>) {
				s{\%.*}{};
				my ($dir,$file) = m{(.*?)/(.*?):};
				$file =~ s/\..*$//;
				next if $dir eq $file;
				$input++ if m{\\input\{$tex_file};
		}
		close GREP;
		return $input;
}


sub clean_utf {
    my $text = shift;
    my $ret = '';
    my $char_anterior=' ';
    for (0 .. length $$text) {
        my $char = substr($$text,$_,1);
        if (ord($char) == 160 && ord($char_anterior)==194) {
            $ret = substr($ret,0,length($ret)-1);
            next;
        }
        $char_anterior = $char;
        $ret .= $char;
    }
    $$text = $ret;
}

sub strip_html_strip {
    my $file = shift;

    my $file_out = $file;
    $file_out =~ s/\.\w+/.txt/;
    open my $in,'<',$file or die "$! $file";
    my $html = join "",<$in>;
    close $in;

    my $hs = HTML::Strip->new();
    my $text = $hs->parse($html);
    $hs->eof;

    open my $out,'>',$file_out or die "$! $file";
    for my $line (split /\n/,$text ) {
        $line =~ s/\s{4}/\n/g;
#        $line =~ s/\.\s+/.\n/g;
        print $out "$line\n";
    }
    close $out;

    return $file_out;
}
sub convert {
	my $file = shift;
    my $filter = (shift or 'txt:text');

	die "No trobo $file" if !-e $file;

	my @run = ($LIBREOFFICE
				,"--convert-to", $filter
				,"--headless"
				,'--invisible'
				,$file);
	warn join(" ",@run)."\n" if $DEBUG;
	system(@run);
	warn "ERROR $?: $! convertint $file\n"
   			.join(" ",@run)	if $? && $DEBUG;

    my ($ext) = $filter =~ m{(.*):};
    $ext = $filter if !$ext;
	my $file_txt = $file;
    $file_txt ="./$file_txt" if $file_txt !~ m{/};

	$file_txt =~ s{.*/(.*)\.\w+$}{$1.$ext};
	warn "No puc convertir l'arxiu $file_txt\n".join (" ",@run)."\n" if ! -e $file_txt;
	warn "convertit $file a $file_txt\n";# if $DEBUG;
	return $file_txt;
}

sub mira_seccio {
	my $curs = shift or croak "Falta el curs";

    return "Anglès"
        if ($curs =~ /\d(st|nd|rd)/);

    return 'Fem Escola' if $curs =~ /escola solc/;

	my ($seccio) = $curs =~ / de (.*)/;
	return "\u$seccio"	if $seccio;

	($seccio) = $curs =~ /\d\w* (.*)/;
	return "\u$seccio"	if $seccio;

	return "\u$curs";
}

sub dir_seccio {
	my $seccio = shift or die "Falta seccio";

	return 'angles'		if $seccio =~ /angl.*s/i;
	return 'primaria' 	if $seccio =~ /prim.*ria/i;
	return 'parvulari' 	if $seccio =~ /parvulari/i;
	return 'eso' 		if $seccio =~ /eso/i;
	return 'fem_escola' if $seccio =~ /fem escola|escola solc/i;
	confess "No trobo la seccio per '$seccio'";
}


sub pagesof {
	my $seccio = shift or die $!;
	opendir my $dir , dir_seccio($seccio) or die $!." ".dir_seccio($seccio);
	my @arxius = readdir $dir;
	closedir $dir;
	die "No trobo arxius a ".dir_seccio($seccio) if !scalar(@arxius);
	my $darrer;
    for (@arxius) {
		$darrer = $_ if /^\d+.*tex$/;
	}
    $darrer = 0 if !$darrer;
#	die "No trobo darrer a @arxius" if !$darrer;
	$darrer =~ s/(\d+).*/$1/;
    $darrer += prefix_seccio($seccio)*100   if $darrer < 100;
	return $darrer + 5;

}

sub traspassa_old {
	my $file = shift;
    my $file_html=convert($file,'html');
	die "No puc convertir $file a $file_html" 
            if !$file_html || ! -e $file_html;
    my $file_txt = strip_html($file_html);
	die "No puc convertir $file_html a $file_txt" 
            if !$file_txt || ! -e $file_txt;

    extreu_imatges($file);
#    $Parasolc::DEBUG=1;
	my $file_tex = crea_tex($file,$file_txt);

    die "No creat arxiu tex per a '$file_txt'"
        if !$file_tex || ! -e $file_tex;

    warn "creat '$file_tex'\n";

    if (!$DEBUG) {
        unlink $file_txt    or die "$! $file_txt";
    }

    return $file_tex;
}

sub extreu_imatges {
    my $file = shift;

    my $file_pdf = convert($file,'pdf');
    my ($root) =  $file =~ m{.*/(.*)\.\w+$};

    my $cwd = getcwd();
    mkdir "tmp" if ! -e "tmp";
    chdir "tmp" or die $!;

    mkdir $root if ! -d $root;
    chdir $root or die "$! $root";

    my @cmd = ('pdfimages','-j', "../../$file_pdf" , $root);

    my ($in, $out, $err);
    run(\@cmd,\$in,\$out,\$err);
    die "Error $? $err $out" if $?;

    chdir $cwd or die "$! $cwd";
}

sub copia_fotos {
	my ($doc,$file_tex) = @_;

	my ($dir_doc, $file_doc) = $doc =~ m{(.*)/(.*)\.\w+};

	copia_fotos_dir($dir_doc,$file_tex);
    copia_fotos_dir("tmp/$file_doc", $file_tex);
    copia_fotos_html($doc,$file_tex);
}

sub copia_fotos_html {
    my ($doc,$file_tex) = @_;

    my @fotos;
    my $count = 0;
    my ($filtre) =  $doc =~ m{.*/(.*)\.\w+$};
    die "No trobo el filtre a '$doc'"   if !$filtre;
	my ($dir_tex,$prefix) = $file_tex =~ m{(.*)/\d+_(.*)\.tex};

    my $dir_pdf = $doc;
    $dir_pdf =~ s{.*/(.*)}{tmp/$1};
    $dir_pdf =~ s/\.\w+//;
    $dir_pdf =~ s/\s+/_/g;

    opendir my $ls, '.' or die $! ;
    while (my $foto = readdir $ls) {
		next if $foto !~ /^$filtre.*\.(bmp|gif|jpg|jpeg|png)$/i;
        $count++;
		$count = "0".$count if length($count)<2;

		my $orig = "./$foto";
		my $dst = "$dir_tex/img/$prefix"."_".$count.".".lc($1);
        warn "copio $orig -> $dst\n";
		copy($orig,$dst)	or die "$! $orig -> $dst";
		push @fotos,($dst);
        warn "esborro $orig";
        unlink $orig or die "$! $orig";
    }
    closedir $ls;
	insereix_fotos($file_tex,\@fotos);
}

sub copia_fotos_dir {
	my $dir_fotos 	= shift or die $!;
	my $file_tex	= shift or die "Falta el file_tex";
	my $count		= shift;
	if (!defined $count) {
		my $first_count = 0;
		$count = \$first_count;
	}
	return if docs_en_dir($dir_fotos)>1;

	my ($dir_tex,$prefix) = $file_tex =~ m{(.*)/\d+_(.*)\.tex};
    confess"No trobo dir tex a $file_tex"  if !$dir_tex;
	warn "Copiant fotos de $dir_fotos\n";

	my @fotos;
	opendir my $dir,$dir_fotos or die "$! $dir_fotos";
	while (my $foto = readdir $dir) {
		next if $foto =~ m{^\.};
		if (-d "$dir_fotos/$foto") {
			copia_fotos_dir("$dir_fotos/$foto",$file_tex,$count);
			next;
		}
		next if $foto !~ /\.(bmp|gif|jpg|jpeg|png)$/i;

		$$count++;
		$$count = "0".$$count if length($$count)<2;

		my $orig = "$dir_fotos/$foto";
		my $dst = "$dir_tex/img/$prefix"."_".$$count.".".lc($1);
		copy($orig,$dst)	or die "$! $orig -> $dst";
		push @fotos,($dst);
	}
	closedir $dir;
	insereix_fotos($file_tex,\@fotos);
    return $count;
}

sub insereix_fotos {
	my ($file_tex,$fotos) = @_;
	copy($file_tex,"$file_tex.backup") 	or die $!;
	open my $in,'<',"$file_tex.backup" 	or die "$! $file_tex.backup";
	open my $out,'>',$file_tex 			or die "$! $file_tex";
	while (my $line = <$in>) {
		print $out $line;
		if ($line =~ /^% curs:/i) {
#			print join "\n",@$fotos;
			print $out "\n"
			    .join("\n\n",
					map { '\noindent\includegraphics[width=\columnwidth'
							.",keepaspectratio]{$_}" } @$fotos)
				."\n\n";
		}
	}
	close $in;
	close $out or die "$! $file_tex";
    unlink "$file_tex.backup" or die "$! $file_tex.backup";
}

sub docs_en_dir {
	my $doc = shift or die "Falta doc";

	my $dir_doc = $doc;
	$dir_doc =~ s{(.*)/.*}{$1};

	die "No trobo dir doc en '$doc'" if !$dir_doc;

    warn "chk docs en '$dir_doc'";
	opendir my $d,$dir_doc or confess "$! $dir_doc";
	my $n_files = 0;
	while (readdir $d) {
		$n_files++ if /\.doc/;
	}
	closedir $d;
	return $n_files;
}

sub docs_sense_tex {
	my ($tex,$doc)  =@_;
	my %doc_trobat;
	for my $tex_file (sort keys %$tex) {
		my $doc_file = busca_doc($tex_file);
		if (!$doc_file || !defined $doc_file->[0]) {
				next if $tex_file =~ m{^portada/}
					|| $tex_file =~ /^$REVISTA/
					|| $tex_file =~ m{.*/\d+_exemple.tex$}
					|| conte_inputs($tex_file)
					|| es_input($tex_file);
				print "  - $tex_file sense font\n";
				next;
		} else {
            for my $doc_file1 (@$doc_file) {
			    if ($doc_trobat{$doc_file1}) {
				    print "Doc $doc_file1 a \n - $tex_file\n"
			   		."	- $doc_trobat{$doc_file1}\n";
			    }
#			    print "Doc trobat $doc_file a $tex_file\n";
			    $doc_trobat{$doc_file1} = $tex_file;
            }
		}
	}

    return if $CHECK_TEX;
	print "Docs sense tex al directori $DIR_FONTS: \n";
	my $cont = 0;
	for my $doc (sort { lc($a) cmp lc($b) }  keys %$doc) {
			next if $doc_trobat{$doc};
			print "$doc\n";
			$cont ++;
            next if $TEST;
			my $file_tex = traspassa($doc);
			copia_fotos($doc,$file_tex);
#			exit;
	}
	print "\t$cont\n";
}

####################################################################
#
busca_seccions();
my %tex;
open TEX,"find . -type f -iname \"*tex\"|" or die $!;
while (<TEX>) {
	next if m{^./output/(bw|lowres|paper)/} || m{.*/exemple.tex}
		|| m{^./lib};
	s{^./(.*)}{$1};
	chomp;
	next if $SECCIO{$_};
	$tex{$_}++;
}
close TEX;

my %doc;
open DOC,"find fonts/ -type f |" or die $!;
while (<DOC>) {
	next if /\.~lock.*\#$/;
	next if !m{.*/.*?\..*};
    next if m{/\.};
    next if /esborrany.pdf/;
    next if !m{.\..};
	next if /\.(jpg|png|gif|css|db|swf|js|html|htm|txt|jpeg)$/i;
	next if /combo.txt/;
	chomp;
	$doc{$_}++;
}
close DOC;


########################################################
#

print "He trobat ".scalar(keys %tex)." tex i ".scalar(keys %doc)." docs\n";

docs_sense_tex(\%tex, \%doc);
