#!/bin/bash
if [ $USER != 'root' ]; then
    echo "has de ser root"
    exit
fi
apt-get install texlive-latex-base texlive-generic-recommended texlive-latex-extra texlive-lang-spanish texlive-xetex texlive-fonts-recommended libhtml-strip-perl libtest-perl-critic-perl
apt-get install imagemagick
apt-get install libimage-size-perl
apt-get install libtext-diff-perl
