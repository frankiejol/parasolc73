#!/usr/bin/perl -w

use strict;
use Cwd;
use File::Copy;
use Getopt::Long;
use Text::Diff;

my $FILE_OUT = "portada/index.tex";
my $MOSTRA_INDEX = 0;

my ($help,$DEBUG);
GetOptions( help => \$help
			,debug => \$DEBUG
			,index => \$MOSTRA_INDEX
);

if ($help) {
			print "$0 [--help] [--debug] [--index]\n";
			exit;
}
my ($REVISTA) = obte_revista_actual();

my @SECCIONS;
my %PLANA;
my %FOTO;
my %REPE;
my %PAGESOF;

sub obte_revista_actual {
    opendir my $ls, '.' or die $!;
    while (my $file = readdir $ls) {
        return $1 if $file =~ /^(parasolc\d+).tex$/;
    }
    close $ls;
    die "No trobo cap parasolcXX.tex";
}

sub busca_seccions {
	open P_TEX,"<$REVISTA.tex" or die "$! $REVISTA.tex";
	while (<P_TEX>) {
		s/#.*//;
		my ($include) = m[\\include\{(.*)/];
		push @SECCIONS, ( $include) if $include;
		print "$0 trobo seccio: $include\n" if $include && $DEBUG;
	}
	close P_TEX;
}

sub inputs {
	my $seccio = shift or die ;
	my @input;
	open SECCIO,"<$seccio/main.tex" or do {
			warn "$! $seccio/main.tex";
		next;
	};
	while (<SECCIO>) {
		s/\%.*//;
		my ($arxiu ) = m[\\input\{(.*)}];
		next if !$arxiu;
		$arxiu .= ".tex" if $arxiu !~ /\.tex$/;
		push @input,($arxiu);
	}
	close SECCIO;
	return @input;
}

sub guarda_index {
	my ($titol,$pagesof,$seccio) = @_;
	return if ! ($titol && $pagesof && $pagesof =~ /^\d+$/ && $pagesof !~ /^0/ );

	warn "WARNING: titol $titol repetit a $seccio [$pagesof]\n"
		if $REPE{$titol}++;

	die "pagesof '$pagesof' repetit a '$PAGESOF{$pagesof}' i '$titol'"
				." en $seccio"
		if $PAGESOF{$pagesof};

	$PAGESOF{$pagesof}=$titol;
	warn '\indexitem{'."$titol}{\\pageref{$pagesof}}\n"
		if $DEBUG;
	if (!$PLANA{$seccio}) {
				$PLANA{lc($seccio)} = $pagesof;
				$FOTO{$seccio} = "portada/img/flower.png";
				my $foto_idx = "$seccio/img/index.png";
				$FOTO{$seccio} = $foto_idx if -e $foto_idx;
	}
}

sub mostra_index {
    print '\begin{'."indexblock}{}\n";
    for (sort { $a <=> $b } keys %PAGESOF) {
#        	print '\indexitem{'.$PAGESOF{$_}."}{\\pageref{$_}}\n";
        print '\indexitem{'.$PAGESOF{$_}."}{$_}\n";
    }
    print '\end{'."indexblock}\n\n";
}

##############################################################

busca_seccions();

open INDEX,">$FILE_OUT.new" or die $!;
select(INDEX);

for my $seccio (@SECCIONS) {

	next if $seccio eq 'portada' || $seccio eq 'contra';
	my @input = inputs($seccio);
	warn "No hi ha inputs per $seccio " if !@input;

	for my $input (@input) {
      next if $input eq 'portada/index.tex';
	  warn "$input\n" if $DEBUG;
	  open SECCIO,"<$input" or die "$! $input";
	  while (<SECCIO>) {
		next if !m[\\begin\{news\}];
		<SECCIO>;
		my $titol=<SECCIO>;
		chomp $titol;
		$titol =~ s/{(.*)}/$1/;
		$titol =~ s/".*?"(.*)/$1/;
		$titol =~ s/\.//;
		my $titol2 = <SECCIO>;
		if ($titol2 =~ /^\%\s*index:\s*(.*?)\s*$/) {
			$titol = $1;
			<SECCIO>;
		}
		<SECCIO>;
		my $pagesof;
		for (1..10) {
			my $line=<SECCIO>;
			chomp $line;
			($pagesof) = $line =~ /\{(\d+)\}/;
			last if $pagesof && $pagesof =~ /^\d+$/;
		}
		warn "Error a '$input'\n'$seccio . $titol', pagesof = '$pagesof'"
			if $pagesof && $pagesof !~ /^\d+$/;
		guarda_index($titol,$pagesof,$seccio);
	  }
	  close SECCIO;
	} # for inputs
}

mostra_index()	if $MOSTRA_INDEX;

print '%titol de les seccions en blanc {}'."\n";
print '\begin{'."weatherblock}{}\n";
for (
        qw(primaria parvulari fem_escola eso )) {
	next if /ampa/;
	next if !$FOTO{$_};
	my $seccio = $_;
	$seccio =~ s/_/ /g;
    $seccio = 'primària' if $seccio eq 'primaria';
	print '\weatheritem{'
		.$FOTO{$_}."}{$seccio}{ \\pageref{".$PLANA{$_}."}}\n";
}
print '\end{'."weatherblock}\n\n";
close INDEX;

my $diff = 1;
$diff = diff $FILE_OUT, "$FILE_OUT.new" if -f $FILE_OUT;
if ($diff) {
	warn "noticies canviat\n";
	copy("$FILE_OUT.new",$FILE_OUT) or die $!;
}
