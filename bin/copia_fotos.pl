#!/usr/bin/perl -w

use strict;
use Cwd;
use Getopt::Long;
use File::Copy;

my %IGNORA = map { $_ => 1 } qw(dels);
my $TEST;

my $help;
my $PREFIX = '';
prefix();
GetOptions( help => \$help, 'prefix=s' => \$PREFIX, test => \$TEST );

my $DESTI = $ARGV[0];

#####################################################################33
#
# neteja prefix
#
sub prefix {
    my ($prefix)= getcwd =~ m{.*/(.*)};
	($prefix) = getcwd =~ m{.*/(.*?)/} if ($prefix =~ /^(foto|fotos|img|image)/i);
    $prefix = lc($prefix);
    $prefix =~ tr/a-z0-9/_/c;
    $prefix =~ s/__+/_/g;
    
	$PREFIX = "";
    for my $paraula ( split /_/,$prefix) {
		next if length($paraula)<3 || $IGNORA{$paraula};
		$PREFIX .= "_" if $PREFIX;
		$PREFIX .= $paraula 
    }

}

if ($help || !$DESTI) {
	print "$0 [--help] [--test] [--prefix=$PREFIX] seccio\n";
	exit;
}

my ($dir) = getcwd();
$dir =~ s{(.*/parasolc\d+)/fonts/.*}{$1};

$DESTI = "$dir/$DESTI/img";

die "No trobo el directori '$DESTI'\n"	if (!-d $DESTI);

$PREFIX="${PREFIX}_" if length $PREFIX;

my $cont = 0;
my %fet;

my $default = 0;
opendir LS,"." or die $!;
while (my $src = readdir LS) {
	next if ! -f $src;
	print "[TEST] " if $TEST;

	print "$src -> ";
	if ($src !~ /\.(jpg|png|gif|bmp)/i) {
			print "SKIPPING\n";
			next;
	}
	my $dst = $src;
	$dst =~ tr/[A-Z]/[a-z]/;
	$dst =~ tr/[A-Za-z0-9.]/_/c;
	$dst =~ s/__+/_/g;
	if ($PREFIX) {
		my ($num) = $dst =~ /.*(\d\d?)/;
		$num=++$default if !$num;
		$num = "0$num" if length($num)<2;
		my ($ext) = $dst =~ /(\..*)$/;
		while ($fet{$num}) {
			$num = ++$cont;
			$num = "0$num" if length($num)<2;
		}
		$fet{$num}++;
		$dst = "$num$ext";
	}
    $dst = "$PREFIX$dst";

    $dst = "$DESTI/$dst";
	$dst =~ s/(.*?)_+(\.\w+)$/$1$2/;
	print " $dst";

	if ( -e $dst ) {
		print " [JA COPIAT]\n";
		next;
	}
	print "\n";
	next if $TEST;
	copy($src,$dst) or die "$! $src -> $dst\n";
}
closedir LS;
