package Parasolc;

use Exporter 'import';
use strict;
use locale;
use utf8;
use Encode;
use HTML::Parser;
use IPC::Run qw(run);
use File::Path qw(mkpath);

our @EXPORT_OK = qw(autor_curs strip_html possible_curs crea_tex prefix_seccio traspassa);

use warnings;
use Carp;

use version; our $VERSION = qv('0.0.3');
our $DEBUG;
my $HTML;
my %SKIP_TAG = map { $_ => 1 } qw(body html head title link meta style);

my $LIBREOFFICE= `which libreoffice`;
chomp $LIBREOFFICE;
die "No trobo libreoffice" if !$LIBREOFFICE;

######################################################################

our $RE_CURS =qr/(prim.*ria|eso|parvulari|escola solc|coordinaci..? escola)/i;



=head2 autor_curs

Retorna l'autor i el curs donada una línia

=cut

sub autor_curs {
    my $linia = shift;
    return if !possible_curs($linia);
    my $pre_autor = shift;
    my $cont = 0;

    $linia =~ s/^\s+//;
	warn "Possible curs a '$linia'\n"   if $DEBUG;
    my ($autor,$curs);

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
    ($curs) = $linia =~ /^\s*(escola\s+solc.*)\s*/i;
    return ($autor or $pre_autor,$curs) if $curs;

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
	($autor,$curs) = $linia =~ /\s*(.*),\s*(\d.*?)\s*$/;
    if ( $curs ) {
        $autor = '' if ! $autor;
        warn "autor: '$autor', curs: '$curs'\n" if $DEBUG;
        return ($autor,$curs);
    }
    warn "intent ".++$cont." $linia\n"  if $DEBUG;
    ($curs) = $linia =~ / de (\d.*?$RE_CURS) /;
    return ($autor or $pre_autor,$curs) if $curs;

    warn "intent lletres '$linia'"  if $DEBUG;
    my ($curs1,$curs2) = $linia =~ /(primer|segon|tercer|quart|cinqu.)\s+de\s+($RE_CURS)/i;
    if ($curs1 && $curs2) {
        $curs1 = lc($curs1);
        $curs2 = lc($curs2);
        $curs2 = 'primària' if $curs2 =~ /prim..?ria/i;
        $curs = "\u$curs1 de \u$curs2";
        $autor = ($pre_autor or '');
        return ($autor ,$curs);
    }

    warn "intent ".++$cont." $linia\n"  if $DEBUG;
	($curs) = $linia =~ /^\s*(.*?)\.?\s*$/	if !$curs;
    return ($autor or $pre_autor,$curs) if $curs;

}

=head2 prefix_seccio

	Numero prefix de la seccio per al pagesof

=cut

sub prefix_seccio {
	my $seccio = shift or die "Falta seccio";

	return 7 	if $seccio =~ /parvulari/i;
	return 5 	if $seccio =~ /prim.*ria/i;
	return 9	if $seccio =~ /eso/i;
    return 4    if $seccio =~ /angl.*s|english/i;
    return 2    if $seccio =~ /fem.escola|escola/i;

    confess "No trobo la seccio per '$seccio'";
}


=head2 possible_curs

Retorna si a la linia actual potser surt el curs

=cut

sub possible_curs {
    my $linia = shift;
    return 1 if defined $linia
            && ( $linia =~ m{ de $RE_CURS}i
                ||$linia =~ m{\d(er|n|t|r)\s+d..?.?ESO}i
				||$linia =~ m{\de?r $RE_CURS}
				||$linia =~ m{\d(st|nd) $RE_CURS}
                ||$linia =~ m{\d(st|nd|rd)(\sof)? $RE_CURS}
                ||$linia =~ m{^\s*escola\s*solc\s*[\d\-\_\s]*$}i
                ||$linia =~ m{^\s*coordinaci.*escola\s*$}i
				) ;
        
    return 0;
}

sub _busca_autor {
    my ($linia, $text, $curs_alt ) = @_;

    warn "Possible autor a '$linia' [curs_alt=".($curs_alt or '<undef>')."]"
        if $DEBUG;
	my $vocals ='ÀÈÌÒÙÁÉÍÓÚàèìòùáéíóúÄËÏÖÜäëïöü';
	my $re =qr/^\s*([$vocals\w]+)\s([$vocals\w]+)\w*([$vocals\w]*)(,.*)?$/li;
	return if $linia !~ /^([$vocals\w]+)\s+([$vocals\w]+)$/li 
        && $linia !~ /$re/;

    warn "Trobat autor\n"   if $DEBUG;
    my ($nom, $cognom1) = ($1, $2);
    my $cognom2 = '';
    $cognom2 = " $3" if $3;
    my $curs = ($4 or $curs_alt);
    $curs =~ s/,\s+//;
    push @$text,("\\authorandplace{$nom $cognom1$cognom2}{$curs}\n");
}

=head2 crea_tex

    Lis passes un arxiu de text i crea un .tex

=cut

sub crea_tex {
	my $file_font = shift or die "Falta file font";
	my $file = shift or die "Falta file";

	my ($titol,$subtitol,$autor,$curs,$curs_alt);
	my @text;

	warn "file: $file\n"	if $DEBUG;
	open my $txt,'<',$file or die "$! $file";
	while (my $linia = <$txt>) {
		next if $linia !~ /\w|\d/;
        warn"$linia\n"  if $DEBUG;
        $linia .= "\n" if $linia =~ m{[.!?]$};
		($curs_alt) = _curs_alternatiu($linia,$curs_alt)    if !$curs_alt;
		if (!$titol || ! defined $subtitol) {
			chomp $linia;
			if (! $titol ) {
                $linia =~ s/^\W*//;
				($titol) = $linia =~ /\s*(.+)\s*/;
				next    if length $titol < 80;
                push @text,("$linia\n");
                ($titol) = $file =~ m{(.*)\.\w+$};
                $subtitol = '';
                next;
			}
            if (!$subtitol ) {
                my $extra;
			    ($subtitol,$extra) = $linia =~ /\s*(.*?)\.\s*(.*)/;
                $subtitol = '' if $subtitol && length $subtitol > 280;
                if ( $subtitol ) {
                    push @text,("$extra\n");
                    next;
                }
            }
			push @text,("$linia\n");
			next;
		}
		chomp $linia;

        next if _busca_autor($linia,\@text, $curs_alt );

		warn "$linia\n" if $DEBUG;
		push @text,($linia);
	}
	close $txt;
	$curs = $curs_alt if !$curs;
    $curs = 'Fem Escola' if !$curs;
	confess("No trobo curs a $file")	if !$curs;
    if (length $titol<4) {
        ($titol) = $file_font =~ m{.*/(.*)\.\w+$};
    }
    $titol =~ s/\&/\\\&/g;
	return _imprimeix_tex($file_font,$titol,$subtitol,$curs,\@text);
}

sub _out {
    my ($arxiu, $txt) = @_;
    open my $out,'>',$arxiu or die "$! $arxiu";
    print $out $txt;
    close $out;
}
sub _convert {
    my $file = shift or die "Falta el doc";
    my $filter = (shift or 'html');

	die "No trobo $file" if !-e $file;

	my @run = ($LIBREOFFICE
				,"--convert-to", $filter
				,"--headless"
				,'--invisible'
				,$file);
	warn join(" ",@run)."\n" if $DEBUG;
	system(@run);
	warn "ERROR $?: $! convertint $file\n"
   			.join(" ",@run)	if $? && $DEBUG;

    my ($ext) = $filter =~ m{(.*):};
    $ext = $filter if !$ext;
	my $file_txt = $file;
    $file_txt ="./$file_txt" if $file_txt !~ m{/};

	$file_txt =~ s{.*/(.*)\.\w+$}{$1.$ext};

    if (!-e $file_txt) {
	    warn "No puc convertir l'arxiu $file_txt\n".join (" ",@run)."\n";
    } else {
	    warn "convertit $file a $file_txt\n" if $DEBUG;
    }
	return $file_txt;
}

sub _curs_alternatiu {
    my $linia = shift;
    my $pre_curs = shift;
    my ($autor,$curs) = autor_curs($linia);
    $curs = $pre_curs if !$curs;
    return $curs;
}

=head2 file_tex

    Retorna el nom d'arxiu tex donat un títol

=cut

sub file_tex {
	my $titol = shift or die "Falta titol";
	my $file = '';
	for (split /\s+/,$titol) {
		next if /^\w+$/ && length($_) <3 && !/\d/;
		$file .= '_' if length $file;
#        tr/áéíóú//;
#        tr/àèìòù//;
		s/[^A-Za-z0-9_]//g;
        s/^\s*(.*?)\s*$/$1/;
        s/^_*//;
		$file .= lc($_); 
        last if length $file > 20;
	}
	$file =~ s/__+/_/g;

    confess "No aconsegueixo generar un filename desde '$titol'"
        if !length$file;
	return $file;
}

sub _file_out {
    my $file_in = shift;
    $file_in =~ s{.*/(.*)\.\w+$}{$1};
    return file_tex($file_in);
}

sub _pagesof {
	my $seccio = shift or die $!;
	opendir my $dir , dir_seccio($seccio) or do {
            mkdir dir_seccio($seccio) or die "$! ".dir_seccio($seccio);
            return prefix_seccio($seccio)*100;
    };
	my @arxius = readdir $dir;
	closedir $dir;
	die "No trobo arxius a ".dir_seccio($seccio) if !scalar(@arxius);
	my ($darrer);
    for (@arxius) {
        my ($trobat) =/^(\d+).*tex$/;
        $darrer = $trobat if $trobat && (!$darrer || $trobat > $darrer);
	}
    $darrer = 0 if !$darrer;
#	die "No trobo darrer a @arxius" if !$darrer;
	$darrer =~ s/(\d+).*/$1/;
    $darrer += prefix_seccio($seccio)*100   if $darrer < 100;
	return $darrer + 5;

}


sub _imprimeix_tex {

	my ($file_font,$titol,$subtitol,$curs,$text) = @_;

    warn "imprimint tex de '$file_font'"    if $DEBUG;
    confess "Necessito el titol" if !$titol;

	my ($seccio) = $file_font =~ m{fonts/(.*?)/};
#    $seccio = mira_seccio($curs)        if !$seccio;
    die "No trobo la seccio de $file_font" if !$seccio;

    $seccio = _maquea_seccio($seccio);
	my $pagesof = _pagesof($seccio);

	my $file_tex = dir_seccio($seccio).
					"/$pagesof".'_'._file_out($file_font).'.tex' ;
	open my $tex,'>',$file_tex
			or die "$! $file_tex";
	print $tex encode('utf8',"")
		."%doc: $file_font\n"
		."\\newpage\n"
		."\n"
		.'\begin{news}'."\n"
		.'{2} %columnes'."\n"
		."{".($titol or '[undef]')."} % titol\n"
		."{".($subtitol or '[undef]')."} % subtitol\n"
		."{$seccio} % seccio\n"
		."{$pagesof} %pagesof\n"
		."% curs: ".encode('utf8',($curs or '[undef]'))."\n"
		."\n"
		.join("\n",@$text)
		."\n";

	print $tex "\n"
		.'\end{news}'."\n"
		;
	close $tex;
    print "traspassat $file_tex\n\n";
	return $file_tex;
}

sub traspassa {
    my $font = shift;

    my $file_html = _convert($font);
    my $file_txt = strip_html($file_html);
    _check_file_utf($file_txt);

    my $file_tex = crea_tex($font, $file_txt);
    _check_file_utf($file_tex);

    return $file_tex;

}

sub _check_file_utf {
    my $file = shift;
    my ($in,$out,$err);
    run(["file",$file],\$in,\$out,\$err);
    die "L'arxiu $file no hauria de ser Non-ISO: $out" 
        if $out =~ /Non-ISO/;
    die "L'arxiu $file hauria de ser _UTF8: $out" 
        if $out !~ /UTF-8/i;
}

sub clean_chars {
    my $text = shift;
    my $ret = '';
    for ( 0 .. length $$text) {
        my $char = substr($$text,$_,1);
#        if ($$text =~ /^T/) {
 #           warn ord($char)." '$char'\n";
            $char = ' ' if ord($char) == 65533;
 #           warn ord($char)." '$char'\n";
   
#        }
        $ret .= $char;
    }
    $$text = $ret;
}

sub start {
    my $tag = shift;
    return if $SKIP_TAG{$tag};
    my $self = shift;
#    warn "$tag\n"   if $DEBUG;
    $self->handler(text => sub { 
        my $text = shift;
        return if $text !~ /./;
        $text =~ s{\n(\w)}{ $1}gmi;
#        warn "'$text'\n"  if $DEBUG;
        utf8::upgrade($text);
        my $out = 'string';
        $out = 'utf8 ' if utf8::is_utf8($text);
        $out .= " valid " if utf8::valid($text);
        my $string = $text;
        $string = encode("utf8",decode("utf8",$text));
        clean_chars(\$string);# if $text =~ /TT/;
        $string.= "\n\n" if $text =~ m{[.!?]\s*$};
        $HTML .= $string;
    }
                    , "dtext");
#    $self->handler(end  => sub { shift->eof} );
    $HTML .= "\n" if $tag eq 'p';
}

sub show_info {
    my $text = shift;
    my $out = "$text ";
    $out .= 'utf8 ' if utf8::is_utf8($text);
    $out .= "valid " if utf8::valid($text);
    $out .= 'length: '.length($text);
    return $out;
}

sub strip_html_parser {
    my $file = shift;
    my $dir_out = shift;

    my $filename = $file;
    $filename =~ s{.*/}{};
    ($dir_out) = $file =~ m{(.*)/} if !$dir_out;

#    die "No trobo die out de $file" if !$dir_out;
    $dir_out = "." if !$dir_out;   

    mkpath($dir_out) or die "$! $dir_out" if ! -e $dir_out;

    my $file_out = "$dir_out/$filename";
    $file_out =~ s{\.html$}{\.txt};

#    binmode($HTML);
    $HTML = '';
    my $p = HTML::Parser->new(
        api_version => 3,
        start_h=> [\&start,"tagname, self"]
#        ,end_h  => [\&end,"tagname"]
    );
    $p->parse_file($file);
    open my $out,'>',$file_out or confess "$! $file_out";
    print $out $HTML;
    close $out;
    
    return $file_out;
}

sub _contents {
    my $file = shift;
    open my $in,'<',$file or die "$! $file";
    return join("",<$in>);
}

sub strip_html {
    return strip_html_parser(@_);
}
=head2 mira_seccio

    Retorna la seccio d'un curs

=cut

=pod

sub mira_seccio {
	my $curs = shift or croak "Falta el curs";

    return "Anglès"
        if ($curs =~ /\d(st|nd|rd)/);

    return 'Fem Escola' if $curs =~ /escola solc|fem escola/i;

	my ($seccio) = $curs =~ / de (.*)(\W|$)/i;
	return _maquea_seccio($seccio)	if _seccio_valida($seccio);

	($seccio) = $curs =~ /\d\w* .*(prim..?ria|ESO|parvulari)/;
	return _maquea_seccio($seccio)  if _seccio_valida($seccio);

    die "No se a quina seccio pertany '$curs'";
}

=cut

sub _maquea_seccio {
    my $seccio = shift;
    $seccio = lc($seccio);
    $seccio =~ tr/À/à/;
    $seccio = uc($seccio) if $seccio =~ /eso/i;
    return "\u$seccio";
}

sub _seccio_valida {
    my $seccio = shift;
    return 0 if !$seccio;
    return 1 if $seccio =~ /^Prim..?ria$/i;
    return 1 if $seccio =~ /^ESO$/i;
    return 1 if $seccio =~ /^Angl..?s$/i;
    return 1 if $seccio =~ /^Fem Escola$/i;
}

=head2 dir_seccio

    Diu a quin directori pertany una seccio

=cut

sub dir_seccio {
	my $seccio = shift or die "Falta seccio";

	return 'angles'		if $seccio =~ /angl.*s|english/i;
	return 'primaria' 	if $seccio =~ /prim.*ria/i;
	return 'parvulari' 	if $seccio =~ /parvulari/i;
	return 'eso' 		if $seccio =~ /eso/i;
	return 'fem_escola' if $seccio =~ /escola/i;
	confess "No trobo la seccio per '$seccio'";
}

1; # Magic true value required at end of module
__END__

=head1 NAME

Parasolc - [One line description of module's purpose here]


=head1 VERSION

This document describes Parasolc version 0.0.1


=head1 SYNOPSIS

    use Parasolc;

=for author to fill in:
    Brief code example(s) here showing commonest usage(s).
    This section will be as far as many users bother reading
    so make it as educational and exeplary as possible.
  
  
=head1 DESCRIPTION

=for author to fill in:
    Write a full description of the module and its features here.
    Use subsections (=head2, =head3) as appropriate.


=head1 INTERFACE 

=for author to fill in:
    Write a separate section listing the public components of the modules
    interface. These normally consist of either subroutines that may be
    exported, or methods that may be called on objects belonging to the
    classes provided by the module.


=head1 DIAGNOSTICS

=for author to fill in:
    List every single error and warning message that the module can
    generate (even the ones that will "never happen"), with a full
    explanation of each problem, one or more likely causes, and any
    suggested remedies.

=over

=item C<< Error message here, perhaps with %s placeholders >>

[Description of error here]

=item C<< Another error message here >>

[Description of error here]

[Et cetera, et cetera]

=back


=head1 CONFIGURATION AND ENVIRONMENT

=for author to fill in:
    A full explanation of any configuration system(s) used by the
    module, including the names and locations of any configuration
    files, and the meaning of any environment variables or properties
    that can be set. These descriptions must also include details of any
    configuration language used.
  
Parasolc requires no configuration files or environment variables.


=head1 DEPENDENCIES

=for author to fill in:
    A list of all the other modules that this module relies upon,
    including any restrictions on versions, and an indication whether
    the module is part of the standard Perl distribution, part of the
    module's distribution, or must be installed separately. ]

None.


=head1 INCOMPATIBILITIES

=for author to fill in:
    A list of any modules that this module cannot be used in conjunction
    with. This may be due to name conflicts in the interface, or
    competition for system or program resources, or due to internal
    limitations of Perl (for example, many modules that use source code
    filters are mutually incompatible).

None reported.


=head1 BUGS AND LIMITATIONS

=for author to fill in:
    A list of known problems with the module, together with some
    indication Whether they are likely to be fixed in an upcoming
    release. Also a list of restrictions on the features the module
    does provide: data types that cannot be handled, performance issues
    and the circumstances in which they may arise, practical
    limitations on the size of data sets, special cases that are not
    (yet) handled, etc.

No bugs have been reported.

Please report any bugs or feature requests to
C<bug-parasolc@rt.cpan.org>, or through the web interface at
L<http://rt.cpan.org>.


=head1 AUTHOR

Francesc Guasch  C<< <frankie@etsetb.upc.edu> >>


=head1 LICENCE AND COPYRIGHT

Copyright (c) 2013, Francesc Guasch C<< <frankie@etsetb.upc.edu> >>. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>.


=head1 DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENCE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.
