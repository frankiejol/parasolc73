use Test::More;# tests => 10;

use strict;
use warnings;

use IPC::Run qw( run );

use_ok('Parasolc');

sub test_out_html {
    my $font = shift;

    my $file_out = $font;
    $file_out =~ s{.*/(.*)\.\w+$}{$1.html};

    if ( -e $file_out ) {
        unlink $file_out or die "$! rm $file_out";
    }

    my $file_html = Parasolc::_convert($font);
    ok($file_html);
    ok($file_html eq $file_out);
    ok(-e $file_html,"Falta out $file_html") or return;
    return $file_html;
}

sub test_out_txt {
    my $font_html = shift;

    my $file_out = $font_html;
    $file_out =~ s{(.*)\.\w+$}{./$1.txt};

    if ( -e $file_out ) {
        unlink $file_out or die "$! rm $file_out";
    }

    my $file_txt = Parasolc::strip_html($font_html);

    ok($file_txt) or return;
    ok($file_txt eq $file_out,"Expecting $file_out, got $file_txt");
    ok(-e $file_txt) or return;

    my ($in,$out,$err);
    run(["file",$file_txt],\$in,\$out,\$err);
    ok($out !~ /Non-ISO/i,$out);
    ok($out =~ /UTF-8/i,$out);
    return $file_txt;
}

sub test_out_tex {
    my $font_doc = shift;
    my $font_txt = shift;
    my $file_out = shift;

    if ( -e $file_out ) {
        unlink $file_out or die "$! rm $file_out";
    }

    my $file_tex = Parasolc::crea_tex($font_doc, $font_txt);
    ok($file_tex) or return;

    ok($file_tex eq $file_out,"Expecting $file_out, got $file_tex");
    ok(-e $file_tex) or return;

    my ($in,$out,$err);
    run(["file",$file_tex],\$in,\$out,\$err);
    ok($out !~ /Non-ISO/i,$out) or return;
    ok($out =~ /UTF-8/i,$out)   or return;
    return $file_tex;

}

sub test_tapies {
    my $font = "fonts/primaria/TÀPIES A PRIMER DE PRIMÀRIA.docx";
    my $exp_tex = "primaria/505_tpies_primer_primria.tex";
    my $out_html = test_out_html($font) or return;

    my $out_txt = test_out_txt($out_html) or return;

    my $out_tex = test_out_tex($font,$out_txt, $exp_tex) or return;

    unlink $out_html;
    unlink $out_txt;
    unlink $out_tex;

}
################################################################33
#

test_tapies();
done_testing();
